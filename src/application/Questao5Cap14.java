package application;

interface MyRes {
	boolean resultado(int x);
}

public class Questao5Cap14 {

	public static void main(String[] args) {
		MyRes estaEntre = (x) -> (x > 10 && x < 20);

		int valor = 151;
		boolean res = estaEntre.resultado(valor);

		if (res) {
			System.out.println("O valor está entre 10 e 20");
		} else {
			System.out.println("O valor não está entre 10 e 20");
		}
	}
}
