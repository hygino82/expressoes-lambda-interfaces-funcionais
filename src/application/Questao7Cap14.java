package application;

interface NumericFuncao {
	int valor(int x);
}

public class Questao7Cap14 {

	public static void main(String[] args) {
		NumericFuncao fatorial = x -> {
			int fat = 1;
			for (int i = 1; i <= x; i++) {
				fat *= i;
			}
			return fat;
		};

		int val = 6;
		int resultado = fatorial.valor(val);
		System.out.printf("O fatorial de %d ser %d\n", val, resultado);
	}
}
