package application;

interface MyFunc {
	int func(int n);
}

interface MyTransform<T extends Number> {
	void transform(T[] a);
	
	
}

public class VarCapture {

	public static void main(String[] args) {

		int num = 10;

		MyFunc mylambda = (n) -> {
			int v = num + n;
			return v;
		};

		System.out.println(mylambda.func(8));
		// num=17; causa erro pois num se comporta como final

		MyTransform<Double> sqrts = (v) -> {
			for (int i = 0; i < v.length; i++)
				v[i] = Math.sqrt(v[i]);
		};
	}
	
	public static void mostrarVetor(Double[] vet) {
		for (int i = 0; i < vet.length; i++) {
			System.out.printf("%d\t", vet[i]);
		}
		System.out.println();
	}

}
