package application;

interface MinhaFuncao<T> {
	T func(T val);
}

public class Questao8Cap14 {

	public static void main(String[] args) {
		MinhaFuncao<String> retornaStringUppercase = (x -> x.toUpperCase());

		String str = retornaStringUppercase.func("Dilma Opressora");
		System.out.println(str);

		MinhaFuncao<Double> elevaQuadrado = (x) -> Math.pow(x, 2.0);

		double valor = 7.0;
		double res = elevaQuadrado.func(valor);
		System.out.printf("%.4f^2 = %.4f\n", valor, res);
	}
}
