package application;

interface StringMod {
	String changeStr(String val);
}

public class Questao9Cap14 {

	public static void main(String[] args) {
		StringMod removeEspacos = (str) -> str.replaceAll("\\s+", "");

		String entrada = "A Dilma ser uma pessoa Opressora";
		String saida = removeEspacos.changeStr(entrada);
		System.out.println(entrada + "\n" + saida);
	}

}
