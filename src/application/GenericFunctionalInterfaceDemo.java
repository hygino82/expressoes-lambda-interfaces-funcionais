package application;

interface SomeTest<T> {
	boolean test(T n, T m);
}

public class GenericFunctionalInterfaceDemo {

	public static void main(String[] args) {
		SomeTest<Integer> isFactor = (n, d) -> (n % d) == 0;

		if (isFactor.test(15, 5)) {
			System.out.println("5 é fator de 15");
		}

		SomeTest<Double> isFactorD = (n, d) -> (n % d) == 0;

		if (isFactorD.test(216.0, 6.0)) {
			System.out.println("6 é fator de 216");
		}

		SomeTest<String> estaDentro = (a, b) -> a.indexOf(b) != -1;

		String str = "Generic Functional Interface";
		System.out.println("Testing string: " + str);

		if (estaDentro.test(str, "face")) {
			System.out.println("'face' is found.");
		} else {
			System.out.println("'face' not found.");
		}
	}

}
