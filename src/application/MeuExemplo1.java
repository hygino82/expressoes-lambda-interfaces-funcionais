package application;

interface Vetor {

	Integer[] teste(Integer[] val);
}

public class MeuExemplo1 {

	public static void main(String[] args) {
		Integer valInt[] = { 1, 2, 3, 4, 5, 6 };
		final int cte=100;

		Vetor vetInverso = (vet) -> {
			Integer inverso[] = new Integer[vet.length];
			for (int i = 0; i < vet.length; i++) {
				inverso[5 - i] = vet[i];
			}
			return inverso;
		};

		Integer[] inversos = vetInverso.teste(valInt);
		System.out.println("Vetor na ordem inversa");
		mostrarVetor(inversos);

		Vetor dobrarValor = (vet) -> {
			Integer dobro[] = new Integer[vet.length];
			for (int i = 0; i < vet.length; i++) {
				dobro[i] = vet[i] * 2;
			}
			return dobro;
		};

		Integer[] valoresDobrados = dobrarValor.teste(valInt);
		System.out.println("O dobro dos valores do vetor");
		mostrarVetor(valoresDobrados);
		
		
	}

	public static void mostrarVetor(Integer[] vet) {
		for (int i = 0; i < vet.length; i++) {
			System.out.printf("%d\t", vet[i]);
		}
		System.out.println();
	}
}
