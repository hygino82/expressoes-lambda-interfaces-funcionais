package application;

@FunctionalInterface
interface IntPredicate {
	boolean test(int n);

}

interface MyArrayCreator<T> {
	T[] func(int n);
}

class MyPredicades {

	static boolean isPrime(int n) {
		if (n < 2) {
			return false;
		}

		for (int i = 2; i <= n / i; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}

	static boolean isEven(int n) {
		return (n % 2) == 0;
	}

	static boolean isPositive(int n) {
		return n > 0;
	}
}

public class MethodRefDemo {

	public static void main(String[] args) {
		boolean result;
		int valor = 37;

		result = numTest(MyPredicades::isPrime, valor);

		if (result) {
			System.out.printf("%d is Prime\n", valor);
		}

		int val2 = 56;
		result = numTest(MyPredicades::isEven, val2);

		if (result) {
			System.out.printf("%d is Even\n", val2);
		}

		int val3 = 147;
		result = numTest(MyPredicades::isPositive, val3);
	}

	static boolean numTest(IntPredicate p, int v) {
		return p.test(v);
	}

}
